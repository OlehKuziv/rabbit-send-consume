﻿using System.Text;
using Common.Models;
using Common.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMqDataCapture.Services;

class Send
{
    public static void Main(string[] args)
    {
        var serviceProvider = new ServiceCollection()
            .AddLogging(l=>l.AddDebug())
            .AddSingleton<ConnectionService>()
            .AddSingleton<MessageBus>()
            .AddSingleton<TransformationService>()
            .AddSingleton<FileSystemWatcherHelper>()
            .Configure<RabbitMQOptions>(x=>new RabbitMQOptions())
            .BuildServiceProvider();
        string argsPath = args.FirstOrDefault();
        string pathOfFolderWithUploadsToWatch = argsPath ?? "/home/oleh/Documents/RabbitTestDocs";
        
        var watcher = serviceProvider.GetService<FileSystemWatcherHelper>();
        watcher.StartAsync(pathOfFolderWithUploadsToWatch, CancellationToken.None);
        
        var transformationService = serviceProvider.GetService<MessageBus>();
        Console.ReadLine();
    }
}
