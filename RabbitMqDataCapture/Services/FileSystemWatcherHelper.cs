using Microsoft.Extensions.Hosting;

namespace RabbitMqDataCapture.Services;

public class FileSystemWatcherHelper:IHostedService
{
    public EventHandler<FileSystemEventArgs> OnFolderChangedHandler;
    private void RegisterWatcher(string directoryToWatch)
    {
        FileSystemWatcher watcher = new FileSystemWatcher();
        watcher.Path = directoryToWatch;
        watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
        
        // Add filter for files of specific extension if you need to.
        //watcher.Filter = "*.odt";

        watcher.Created += new FileSystemEventHandler(OnChanged);
        watcher.EnableRaisingEvents = true;
    }
    
    private void OnChanged(object source, FileSystemEventArgs e)
    {
        OnFolderChangedHandler?.Invoke(this, e);
    }

    public Task StartAsync(string path, CancellationToken cancellationToken)
    {
        RegisterWatcher(path);
        return StartAsync(cancellationToken);
    }
    public Task StartAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        OnFolderChangedHandler = null;
        return Task.CompletedTask;
    }
}