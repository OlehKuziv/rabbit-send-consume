using Common.Models;

namespace RabbitMqDataCapture.Services;

public class TransformationService
{
    private readonly FileSystemWatcherHelper _fileSystemWatcherHelper;
    public EventHandler<TypeMessage> OnTypeMessageReadyHandler;

    public TransformationService(FileSystemWatcherHelper fileSystemWatcherHelper)
    {
        _fileSystemWatcherHelper = fileSystemWatcherHelper;
        _fileSystemWatcherHelper.OnFolderChangedHandler += ProcessNewFileUploaded;
    }

    private void ProcessNewFileUploaded(object sender, FileSystemEventArgs fileSystemEventArgs)
    {
        string fileName = fileSystemEventArgs.Name;
        string path = fileSystemEventArgs.FullPath;
        byte[] fileData = File.ReadAllBytes(path);
        var fileMessage = new FileMessage(fileName, fileData);
        var messageType = new TypeMessage(QueueMessageType.FileUpload, fileMessage.Serialze());
        OnTypeMessageReadyHandler?.Invoke(this, messageType);
    }
}