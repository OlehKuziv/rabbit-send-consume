using Common.Models;
using Common.Services;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace RabbitMqDataCapture.Services;

public class MessageBus
{
    private int FiftyMegabytesInBytes = 52428800;
    private readonly TransformationService _transformationService;
    private IModel _consumerChannel;
    private readonly ILogger<MessageBus> _logger;
    private readonly ConnectionService _persistentConnection;
    
    public MessageBus(ILogger<MessageBus> logger, TransformationService transformationService, ConnectionService persistentConnection)
    {
        _transformationService = transformationService;
        _transformationService.OnTypeMessageReadyHandler += PublishMessageToEventBus;
        _persistentConnection = persistentConnection;
        _logger = logger;
        _consumerChannel = SetUpConsumerChannel();
    }
  
    private IModel SetUpConsumerChannel()
    {
        _logger.LogInformation("Starting setup of RabbitMQ consumer channel");
        if (!_persistentConnection.IsConnected)
        {
            _persistentConnection.TryConnect();
        }
        var consumerChannel = _persistentConnection.CreateModel();
        consumerChannel.CallbackException += (sender, ea) =>
        {
            _logger.LogWarning("Recreating RabbitMQ consumer channel");
            _consumerChannel.Dispose();
            _consumerChannel = SetUpConsumerChannel();
        };
        return consumerChannel ?? SetUpConsumerChannel();
    }

    
    private void PublishMessageToEventBus(object? sender, TypeMessage e)
    {
        if (e.Payload.Length > FiftyMegabytesInBytes)
        {
            PublishMessageSequenceToEventBus(e, 0, Guid.NewGuid().ToString(), 0);
        }
        else
        {
            PublishMessageToEventBusInternal(e);
        }
    }

    private void PublishMessageSequenceToEventBus(TypeMessage e, int bufferPosition, string sequenceCorrelationId,
        int sequencePositionId)
    {
        var messagesNumber = Math.Ceiling(e.Payload.Length / (double) FiftyMegabytesInBytes);
        if (bufferPosition >= e.Payload.Length && sequencePositionId == messagesNumber)
        {
            return;
        }

        byte[] messagePayload = GetPayloadForMessageSequence(e.Payload, bufferPosition);
        
        MessageSequence currentMessageFromSequence = new MessageSequence(messagePayload, sequenceCorrelationId, sequencePositionId, (int)messagesNumber);
        PublishMessageSequenceToEventBusInternal(e.QueueMessageType, currentMessageFromSequence);
        PublishMessageSequenceToEventBus(e, bufferPosition + messagePayload.Length, sequenceCorrelationId, sequencePositionId + 1);

    }

    private byte[] GetPayloadForMessageSequence(byte[] e, int bufferPosition)
    {
        int bytesToRead = e.Length - bufferPosition > FiftyMegabytesInBytes ? FiftyMegabytesInBytes : e.Length - bufferPosition;
        byte[] messagePayload = new byte[bytesToRead];
        using (MemoryStream m = new MemoryStream())
        {
            using (BinaryWriter writer = new BinaryWriter(m))
            {
                writer.Write(e, bufferPosition, messagePayload.Length);
                messagePayload = m.ToArray();
            }
        }
        return messagePayload;
    }
    
    private void PublishMessageToEventBusInternal(TypeMessage e)
    {
        var properties = _consumerChannel.CreateBasicProperties();
        properties.Persistent = true;
        properties.Type = e.QueueMessageType.ToString();
        _logger.LogInformation("Publishing message to RabbitMQ");
        try
        {
            _consumerChannel.BasicPublish(exchange: "",
                routingKey: "testFileQueue",
                basicProperties: properties,
                body: e.Payload);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
    
    private void PublishMessageSequenceToEventBusInternal(QueueMessageType messageType, MessageSequence currentMessageFromSequence)
    {
        var properties = _consumerChannel.CreateBasicProperties();
        properties.Persistent = true;
        properties.Type = messageType.ToString();
        properties.CorrelationId = currentMessageFromSequence.SequenceCorrelationId;

        Dictionary<string, object> sequenceProperties = new Dictionary<string, object>()
        {
            {nameof(currentMessageFromSequence.PositionsTotal), currentMessageFromSequence.PositionsTotal},
            {nameof(currentMessageFromSequence.SequencePositionId), currentMessageFromSequence.SequencePositionId},
        };

        properties.Headers = sequenceProperties;
        _logger.LogInformation("Publishing message to RabbitMQ");
        try
        {
            _consumerChannel.BasicPublish(exchange: "",
                routingKey: "testFileQueue",
                basicProperties: properties,
                body: currentMessageFromSequence.Payload);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}