using Common.Models;

namespace RabbitMqReceiver.Services;

public static class FileWriter
{
    public static void WriteFile(string path, FileMessage fileMessage)
    {
        path = string.Join('/', path, fileMessage.Name);
        try
        {
            using (FileStream fs = File.Create(path))
            {
                fs.Write(fileMessage.Body, 0, fileMessage.Body.Length);
                fs.Flush();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
}