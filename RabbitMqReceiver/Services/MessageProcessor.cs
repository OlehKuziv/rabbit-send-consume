using Common.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMqReceiver.Services;

public class MessageProcessor
{
    private readonly string _pathForProcessedFiles;
    private readonly Dictionary<string, Aggregate> _sequenceAggregates;

    public MessageProcessor(string pathForProcessedFiles)
     {
         _pathForProcessedFiles = pathForProcessedFiles;
         _sequenceAggregates = new Dictionary<string, Aggregate>();
     }
    
     public void SetupProcessor()
     {
         var factory = new ConnectionFactory() { HostName = "localhost" };
         using(var connection = factory.CreateConnection())
         using(var channel = connection.CreateModel())
         {
             channel.QueueDeclare(queue: "testFileQueue",
                 durable: false,
                 exclusive: false,
                 autoDelete: false,
                 arguments: null);

             var consumer = new EventingBasicConsumer(channel);
             consumer.Received += (model, ea) =>
             {
                 ProcessMessage(ea);
             };
             channel.BasicConsume(queue: "testFileQueue",
                 autoAck: true,
                 consumer: consumer);

             Console.WriteLine(" Press [enter] to exit.");
             Console.ReadLine();
         }
     }
     
    private void ProcessMessage(BasicDeliverEventArgs ea)
    {
        if (IsSequenceMessage(ea))
        {
            ProcessSequenceMessage(ea);
        }
        else
        {
            ProcessSingleMessage(ea.Body.ToArray(), ea.BasicProperties.Type);
        }
    }

    private void ProcessSingleMessage(byte[] messagePayload, string queueMessageType)
    {
        if (queueMessageType.Equals(QueueMessageType.FileUpload.ToString()))
        {
            FileMessage fileMessage = new FileMessage(messagePayload);
            Console.WriteLine($"Receiver is processing new message {fileMessage.Name}");
            FileWriter.WriteFile(_pathForProcessedFiles, fileMessage);
        }
    }
    private void ProcessSequenceMessage(BasicDeliverEventArgs ea)
    {
        int positionsTotal = int.Parse(ea.BasicProperties.Headers[nameof(MessageSequence.PositionsTotal)].ToString());
        if (!_sequenceAggregates.TryGetValue(ea.BasicProperties.CorrelationId, out Aggregate aggregate))
        {
            aggregate = new Aggregate(positionsTotal, ea.BasicProperties.Type);
            _sequenceAggregates.Add(ea.BasicProperties.CorrelationId, aggregate);
        }
           
        int sequencePosition = int.Parse(ea.BasicProperties.Headers[nameof(MessageSequence.SequencePositionId)].ToString());
         
        aggregate.AddMessage(new MessageSequence(ea.Body, ea.BasicProperties.CorrelationId, sequencePosition, positionsTotal));
        if (aggregate.IsComplete())
        {
            ProcessSingleMessage(aggregate.GetCompletePayloadForTheSequence(), aggregate.SequenceQueueMessageType);
        }
    }
    
    private bool IsSequenceMessage(BasicDeliverEventArgs ea)
    {
        return !string.IsNullOrEmpty(ea.BasicProperties.CorrelationId);
    }
}