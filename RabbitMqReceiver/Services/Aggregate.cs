using Common.Models;

namespace RabbitMqReceiver.Services;

public class Aggregate
{
    private readonly MessageSequence[] _messageSequenceParts;
    public Aggregate(int expectedMessageCount, string queueMessageType)
    {
        SequenceQueueMessageType = queueMessageType;
        _messageSequenceParts = new MessageSequence[expectedMessageCount];
    }

    public string SequenceQueueMessageType { get;}

    public void AddMessage(MessageSequence message)
    {
        if (message.SequencePositionId >= _messageSequenceParts.Length)
        {
            throw new Exception();
        }
        if (message.PositionsTotal != _messageSequenceParts.Length)
        {
            throw new Exception();
        }
        _messageSequenceParts[message.SequencePositionId] = message;
    }

    public bool IsComplete()
    {
        return _messageSequenceParts.All(x => x != null);
    }
    
    
    public byte[] GetCompletePayloadForTheSequence()
    {
        if (IsComplete())
        {
            using (MemoryStream m = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(m))
                {
                    for (int position = 0; position < _messageSequenceParts.Length; position++)
                    {
                        writer.Write(_messageSequenceParts[position].Payload);
                    }
                    writer.Flush();
                }
                return m.ToArray();
            }
        }
        return null;
    }
}