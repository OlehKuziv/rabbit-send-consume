﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMqReceiver.Services;

namespace RabbitMqReceiver;

class Program
{
    public static void Main(string[] args)
    {
        string argsPath = args.FirstOrDefault();
        string pathWhereToSaveReceivedFiles = argsPath??"/home/oleh/RiderProjects/RabbitMqDataCapture";
        
        var serviceProvider = new ServiceCollection()
            .AddLogging(l => l.AddDebug())
            .AddSingleton<MessageProcessor>(new MessageProcessor(pathWhereToSaveReceivedFiles)) // better to inject ioptions but the purpose it the same
            .BuildServiceProvider();
        var messageProcessor = serviceProvider.GetService<MessageProcessor>();
        messageProcessor.SetupProcessor();
        Console.ReadLine();
    }
}