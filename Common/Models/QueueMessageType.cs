namespace Common.Models;

public enum QueueMessageType
{
    FileUpload,
    Metadata
}