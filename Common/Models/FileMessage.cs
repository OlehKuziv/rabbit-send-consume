using System.Net.Mime;
using System.Text;

namespace Common.Models;

public class FileMessage:Message
{
    public string Name { get; }
    public byte[] Body { get; }
    public FileMessage(string name, byte[]body)
    {
        Name = name;
        Body = body;
    }
    
    public FileMessage(byte[] msg)
    {
        FileMessage result;
        using (MemoryStream m = new MemoryStream(msg)) {
            using (BinaryReader reader = new BinaryReader(m))
            {
                
                int fileNameSize = reader.ReadInt32();
                byte[] nameBytes = reader.ReadBytes(fileNameSize);
                //string nameBase64 = Convert.ToBase64String(nameBytes);
                string originalName = Encoding.UTF8.GetString(nameBytes);//DecodeFromBase64(nameBase64);
                byte[] body = reader.ReadBytes((int) (reader.BaseStream.Length - reader.BaseStream.Position));
                result = new FileMessage(originalName, body);
            }
        }
        this.Body = result.Body;
        this.Name = result.Name;
    }

    public byte[] Serialze()
    {
        //string base64String = EncodeTo64(Name);
        byte[] nameBytes = Encoding.UTF8.GetBytes(Name);
        using (MemoryStream m = new MemoryStream()) {
            using (BinaryWriter writer = new BinaryWriter(m)) {
                //writer.Write(base64String.Length);
                //writer.Write(base64String);
                writer.Write(Name.Length);
                writer.Write(nameBytes);
                writer.Write(Body);
                writer.Flush();
            }
            return m.ToArray();
        }
    }
    
     public string EncodeTo64(string toEncode)
     {
         byte[] toEncodeBytes = UTF8Encoding.UTF8.GetBytes(toEncode);
         string returnValue = Convert.ToBase64String(toEncodeBytes);
         return returnValue;
     }
     
     public string DecodeFromBase64(string toDecode)
     {
         byte[] toDecodeBytes = Convert.FromBase64String(toDecode);
         return UTF8Encoding.UTF8.GetString(toDecodeBytes);;
     }
}