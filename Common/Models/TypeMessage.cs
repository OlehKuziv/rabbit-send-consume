﻿namespace Common.Models;

public class TypeMessage
{
    public TypeMessage(QueueMessageType queueMessageType, byte[] payload)
    {
        QueueMessageType = queueMessageType;
        Payload = payload;
    }
    public byte[] Payload { get; }
    public QueueMessageType QueueMessageType { get; }
}