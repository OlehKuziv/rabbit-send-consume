namespace Common.Models;

public class MessageSequence
{
    public MessageSequence(ReadOnlyMemory<byte> payload, string sequenceCorrelationId, int sequencePositionId, int positionsTotal)
    {
        SequenceCorrelationId = sequenceCorrelationId;
        SequencePositionId = sequencePositionId;
        Payload = payload.ToArray();
        PositionsTotal = positionsTotal;
    }

    public byte[] Payload { get; }
    public string SequenceCorrelationId { get; }
    public int SequencePositionId { get; }
    public int PositionsTotal { get; }
}